# Change Log
All notable changes to this project will be documented in this file.


## 0.1.1 - 2016-03-29
### Added
- Added support for deploying swagger definition files from auth apache container.

### Changed
- No change.

### Removed
- No change.

## 0.1.0 - 2016-03-19
### Added
- Project CHANGELOG.md file.
- Added support for deploying core APIs using A/B deployment.

### Changed
- Deploying core services now requires a configuration file similar to the tenant config.yml file. See docs for required fields.

### Removed
- No change.