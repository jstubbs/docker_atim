#!/bin/bash
#
# Creates a new apim Docker image using docker-ansible and the roles within this directory.
#

# run docker-ansible using the roles provided here
./build_app.sh apim17 foo centos &&

# commit the docker-ansible image:
./build_app.sh commit apim17

# tag the image
docker tag -f apim17 jstubbs/apim17_base
