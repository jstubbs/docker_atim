#!/bin/bash

./template_compiler/template_entry.sh

# append the container hosts file with the content created by ansible:
cat /tmp/hosts >> /etc/hosts

# link in the mounted apis
ln -s /apis/* /wso2am-1.9.0/repository/deployment/server/synapse-configs/default/api/

# start apim service:
cmd=$(/etc/init.d/apim start $*)
echo "$cmd"
#/etc/init.d/apim start

sleep infinity