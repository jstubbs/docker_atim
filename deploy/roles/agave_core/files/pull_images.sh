#!/bin/bash

ARG=$1

if [ "$ARG" = "" ]; then
  ARG=nightly
fi

docker pull agaveapi/apache-api-proxy:alpine
docker pull agaveapi/jobs-api:$ARG
docker pull agaveapi/files-api:$ARG
docker pull agaveapi/apps-api:$ARG
docker pull agaveapi/systems-api:$ARG
docker pull agaveapi/monitors-api:$ARG
docker pull agaveapi/metadata-api:$ARG
docker pull agaveapi/notifications-api:$ARG
docker pull agaveapi/tramsforms-api:$ARG
docker pull agaveapi/postits-api:$ARG
docker pull agaveapi/usage-api:$ARG
docker pull agaveapi/tenants-api:$ARG
docker pull agaveapi/logging-api:$ARG
