$(document).ready(function(){
    $('#authorizeLink').click(function(){
        $('#loginForm').show('slow');
    });
	$('#login-btn').click(function(){
			var error = "";
			if($('#username').val() == ""){
				error += '<div>Username field is empty.</div>';
			}
			if($('#password').val() == ""){
				error += '<div>Password field is empty.</div>';
			}
			if(error == ""){
				$('#errorMsg').hide('slow');
				$('#loginForm').submit();
				
			}else{				
				$('#errorMsg').html(error).show('slow');
			}
	});
	$('#denyLink').click(function(){
			$('#denyForm').submit();
	});
});
