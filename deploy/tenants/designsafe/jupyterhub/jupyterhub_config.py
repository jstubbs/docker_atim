import os

c.JupyterHub.spawner_class = 'dockerspawner.DockerSpawner'

c.JupyterHub.confirm_no_ssl = True
c.JupyterHub.hub_ip = '172.17.0.1'

c.DockerSpawner.container_ip = '172.17.42.1'
c.DockerSpawner.container_image = 'taccsciapps/jupyter-pyspark-ds'
c.DockerSpawner.remove_containers = True

c.SystemUserSpawner.host_homedir_format_string = '/home/apim'

c.JupyterHub.authenticator_class = 'oauthenticator.AgaveOAuthenticator'

c.AgaveOAuthenticator.oauth_callback_url = os.environ['OAUTH_CALLBACK_URL']
c.AgaveOAuthenticator.client_id = os.environ['AGAVE_CLIENT_ID']
c.AgaveOAuthenticator.client_secret = os.environ['AGAVE_CLIENT_SECRET']
c.AgaveOAuthenticator.agave_base_url = os.environ['AGAVE_BASE_URL']
c.AgaveOAuthenticator.agave_base_url = os.environ['AGAVE_TENANT_NAME']

c.AgaveMixin.agave_base_url = os.environ['AGAVE_BASE_URL']