#!/bin/bash

export ANSIBLE_HOST_KEY_CHECKING=False
cd /ansible; git checkout -b origin/stable-2.0.0.1; git submodule update --init --recursive
source /ansible/hacking/env-setup

ansible-playbook $*